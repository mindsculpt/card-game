import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class CardService {
	private _url: string = 'https://deckofcardsapi.com/api/deck/new/draw/?count=2';

	constructor(private _http: Http) {}

	getCards() {
		return this._http.get(this._url)
			.map((response:Response) => response.json())			
			.catch( this._errorHandler )
	}

	_errorHandler(error: Response) {
		console.error(error);
		return Observable.throw(error || "Server Error");
	}
}