import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.css']
})
export class DealerComponent implements OnInit {
	
	@Input() person;

	constructor() { }

	ngOnInit() {
	}

}
