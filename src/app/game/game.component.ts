import { Component, OnInit } from '@angular/core';
import { CardService } from '../cards.service';
import 'rxjs/add/operator/finally';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  providers: [ CardService ]
})
export class GameComponent implements OnInit {

	// set arrays to store JSON results from cards api call
	public dealer 	= [];
	public player 	= [];
	public cards   = [];

	// set vars to hold response and feedback
	public playerGuess: String;
	public dealerCardValue: String;
	public playerCardValue: String;
	public errorMsg: String;
	public dataLoaded: Boolean;
	public winner: Boolean;
	public showIcon: Boolean;
	public show: Boolean;
	public showGuessButtons: Boolean;
	public result: String;
	public error: String;
	public tryAgain: Boolean;

	// assign values to face cards so we have a numeric value to compare all cards to
	public faceCards = [
		{
			"JACK" : "11",
			"QUEEN" : "12",
			"KING" : "13",
			"ACE" : "14"
		}
	];

	constructor(private _cardService: CardService) { }

	ngOnInit() {


		// draw cards
		this.drawCards(),

		// set local variables (should probably go in their respective components)
		this.dealer = [
			{
				name: 'Michael',
				image: 'michael.jpg'

			}
		],

		this.player = [
			{
				name: 'Berkadia'
			}
		]
	}

	// checks card value to see if it's a face card, and if so, convert it to a numeric value
	checkIfFaceCard(cardValue) {

		if (cardValue.match(/[A-Z]/i)) {

			return this.faceCards[0][cardValue];

		} else {

			return cardValue;
		}
	}

	// reveals card
	revealCard(guess, cardOneValue, cardTwoValue, event) {

		event.stopPropagation();

		// hide buttons and icon
		this.showGuessButtons = !this.showGuessButtons;
		this.showIcon = !this.showIcon;

		// set local values
		this.playerGuess = guess;
		this.dealerCardValue = this.checkIfFaceCard(cardOneValue);
		this.playerCardValue = this.checkIfFaceCard(cardTwoValue);

		// calculate result
		this.caculateResult();
	}

	checkCards() {

		console.log('checking cards...')
		//console.log(this.cards[0].value)
		//console.log(this.cards[1].value)
		//console.log(this.checkIfFaceCard(this.cards[0].value))
		//console.log(this.checkIfFaceCard(this.cards[1].value))

		var cardOne = this.checkIfFaceCard(this.cards[0].value);
		var cardTwo = this.checkIfFaceCard(this.cards[1].value);

		// check if drawn cards have same value, if so, draw again
		if (cardOne == cardTwo) {
			this.drawCards();
		} else {
			// cards drawn have different values, display 'em
			this.dataLoaded = true;
			this.showGuessButtons = !this.showGuessButtons;		
			this.showIcon = !this.showIcon;
		}


	}

	// draws cards from cards.service
	drawCards(){
		
		var root = this;

		// call the api, then on complete check the cards
	  	this._cardService.getCards()
	  		//.finally(() => this.dataLoaded = true)
			.subscribe(
				resCardsData => this.cards = resCardsData.cards,
				resCompleted => this.dataLoaded = resCompleted,
				function() {
					console.log("subscription is completed")
					root.checkCards();
				}
			);
	}

	// resets cards and draws them again
	resetCards(){
		this.result = '';
		this.tryAgain = !this.tryAgain;
		this.show = !this.show;
	   	this.drawCards();
	}

	// calculate if the player guessed correctly
	caculateResult(){

		// based off of the guess type, determine if guess was correct/incorrect
	   switch (this.playerGuess) {

	   	case 'lower' :

	   		if (Number(this.playerCardValue) < Number(this.dealerCardValue)) {
			  console.log('lower: correct!');
			  this.result = 'You Win!';
			  this.winner = true;
			} else {
				console.log('lower: incorrect!');
				 this.result = 'Better Luck Next Time';
				 this.winner = false;
			}
	   		break;
	   	;

	   	case 'higher' :
			if (Number(this.playerCardValue) > Number(this.dealerCardValue)) {
			  console.log('higher: correct!');
			  this.result = 'You Win!';
			  this.winner = true;
			} else {
				console.log('higher: incorrect!')
				this.result = 'Better Luck Next Time';				
				this.winner = false;
			}
			break;
	   	;
	   }

	   // reveal card and try again button
	   this.tryAgain = !this.tryAgain;
	   this.show = !this.show;
	}
}
