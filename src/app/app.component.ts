import { Component } from '@angular/core';
import { Http, HttpModule, Response, JsonpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CardService } from './cards.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ CardService ]
})
export class AppComponent {
  title = '';

}