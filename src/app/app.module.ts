import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';
import 'hammerjs';

import { AppComponent } from './app.component';
import { GameModule } from './game/game.module';

@NgModule({
  
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    GameModule,
    HttpModule,
    JsonpModule,
    MaterialModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }